import cv2 # pip install opencv-python
import numpy as np # installed with cv2
import os # should work straight off
import screeninfo # pip install screeninfo

# Setup the display window
fileInputName = "kane.png" 
fileOutputName = "hologram_kane.bmp" 
windowName = "imageWindow" 
screen_id = 1
screen = screeninfo.get_monitors()[screen_id]
os.environ['DISPLAY'] = ':0.2'
cv2.namedWindow(windowName, cv2.WND_PROP_FULLSCREEN)
cv2.moveWindow(windowName, screen.x - 1, screen.y - 1)
cv2.setWindowProperty(windowName, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

# Read in target image
target=cv2.imread(fileInputName,cv2.IMREAD_GRAYSCALE).astype(float)
print(target.shape)

# Scale target image
target=np.divide(target,np.sqrt(np.mean(np.mean(np.square(np.absolute(target))))))

result = np.zeros(target.shape + (3,), dtype=np.uint8);
replay = np.zeros(target.shape, dtype=complex); 
for colorNo in range(3):
    colorSum = np.zeros(target.shape, dtype=np.uint8);
    for bitNo in range(2):
        # Randomise phase
        replay = target*np.exp(2*np.pi*1j*np.random.rand(target.shape[0],target.shape[1]))
        
        # Back propogate
        diffraction=np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(replay)))*np.sqrt(target.shape[0]*target.shape[1])

        for rotNo in range(4):
            # Rotate by quarter turn
            diffraction = diffraction*np.exp(np.pi*1j/2)
            
            # Constrain to SLM
            subFrame = np.uint8((diffraction>0))
            
            # Add to total for this color
            colorSum += np.uint8(subFrame << (bitNo*4+rotNo))
        
    result[:,:,colorNo] = np.uint8(colorSum);
    
# Save the image
cv2.imwrite(fileOutputName,result)

# Show the image
cv2.imshow(windowName,result)
cv2.waitKey()
cv2.destroyAllWindows()
